#!/bin/bash
#create sub-folders

function input(){
	while echo $num | egrep -v "^[0-9]" > /dev/null 2>&1
	do
		echo "Enter num of sub-folders want to create:"
		read num
	done
}

function output(){
	for ((i=1;i<=num;i++))
	do
		 mkdir -p my-folder/current_user_$i
	done
}

#main
input
output $num
