#!/bin/bash
#print number

function input(){
	while echo $num | egrep -v "^[0-9]" > /dev/null 2>&1
	do
		echo "Please enter number of row: "
		read num
	done
}

function output(){
	for ((i=1;i<=num;i++))
	do
		for ((j=1;j<=num;j++))
		 do
			echo -ne "$j\t"
		done
		echo ""
	done
}

#main
input
output $num

release v0.1
