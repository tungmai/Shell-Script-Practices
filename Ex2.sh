#!/bin/bash

#Quadratic Equation
#status: 
#     2 Equation has 2 experimentals
#     1 Equation has 1 experimental
#     0 Equation has no experimental
#     -1 Equation invalid 

function input(){
	while echo $a $b $c | egrep -v "^[0-9]" > /dev/null 2>&1
	do
		echo "Please enter a, b, c: "
		read a
		read b
		read c
	done
}

function output(){
	if [ "$a" -eq 0 ]
	then 
		echo "x1= ; x2= ; status=-1 : Equation invalid"
	else
		delta=`echo "scale=2; ($b*$b) - (4*$a*$c)" | bc`

		if [ "$delta" -lt 0 ]
		then
			echo "x1= ; x2= ; status=0 : Equation has no experimental"

		elif [ "$delta" -eq 0 ]
		then
			x=`echo "scale=2; - $b/(2*$a) "| bc`
			echo "x1=$x; x2=$x; status=1 : Equation has 1 experimental"

		else
			x1=`echo "scale=2;(- $b+sqrt($delta))/(2*$a)" | bc`
			x2=`echo "scale=2;(- $b-sqrt($delta))/(2*$a)" | bc`
			echo "x1=$x1; x2=$x2; status=2 : Equation has 2 experimentals"
		fi
	fi
}

#main
input
output $a $b $c
				 
